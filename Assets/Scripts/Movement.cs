﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Up"))
        {
            transform.Translate(0,1,0);
        }
        if(Input.GetButtonDown("Down"))
        {
            transform.Translate(0,-1,0);
        }
        if(Input.GetButtonDown("Right"))
        {
            transform.Translate(1,0,0);
        }
        if(Input.GetButtonDown("Left"))
        {
            transform.Translate(-1,0,0);
        }
    }
}
